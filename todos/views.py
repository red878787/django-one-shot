from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm

# from todos.forms import TodosForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()

    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    print(todos)

    context = {
        "todo_object": todos,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "todo_list_create": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoListForm(instance=edit)

    context = {
        "edit_list": edit,
        "todo_list_update": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.save()

            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "todo_item_create": form,
    }
    return render(request, "todos/items/create.html", context)
